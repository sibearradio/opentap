<Project Sdk="Microsoft.NET.Sdk">
  <PropertyGroup>
    <TargetFramework>netstandard2.0</TargetFramework>
    <AppendTargetFrameworkToOutputPath>false</AppendTargetFrameworkToOutputPath>
    <CopyLocalLockFileAssemblies>true</CopyLocalLockFileAssemblies>
    <TargetFrameworkIdentifier></TargetFrameworkIdentifier>
    <TargetFrameworkVersion></TargetFrameworkVersion>
    <OpenTapPackageDefinitionPath>package.xml</OpenTapPackageDefinitionPath>
    <CreateOpenTapPackage>false</CreateOpenTapPackage>
  </PropertyGroup>
  
  <PropertyGroup Condition="'$(Configuration)' == 'Release'">
    <CreateOpenTapPackage>true</CreateOpenTapPackage>
  </PropertyGroup>

  <ItemGroup>
    <PackageReference Include="OpenTAP" Version="{0}" />
  </ItemGroup>
</Project>